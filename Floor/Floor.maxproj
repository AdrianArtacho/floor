{
	"name" : "Floor",
	"version" : 1,
	"creationdate" : 3716795283,
	"modificationdate" : 3728627500,
	"viewrect" : [ 378.0, 135.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"Floor.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"ramping.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"areaToggle.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Floor_sandbox.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"viscosity.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"1-Classification.data" : 			{
				"kind" : "audiofile",
				"local" : 1
			}

		}
,
		"externals" : 		{
			"ml.svm.mxo" : 			{
				"kind" : "object",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1835887981,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
