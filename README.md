# Floor #

This is a patch pertaining the installation "The Rocket that bumped off the ceiling", at the Schauspielhaus Wien.

![FloorUI](https://docs.google.com/drawings/d/e/2PACX-1vRGp8NNTGAHPdB493_xfjwpPkYUNP8wmnslE3B3-iprlc54gKFKAnRfYWBtoUFzbm_mIZD2C40okW6J/pub?w=914&h=195)


### Envelope following ###

The process of measuring the 'saliency' of the microphone inputs is rather simple;
Each *Mic bus* has the following tracks (example):

*Mic1*: Audio track whose Input is set for a spcific mic.

*Mic1-CC102*: Audio track that receives the output of the 'Mic1' track. 
Here is where an envelope follower measures the input. 
The envelope follower is 'mapped' to the knob of a
[**Tesser_CC2note**](https://bitbucket.org/AdrianArtacho/tesser_cc2note/src/master/) device, whose
settings are set to convert it to a midi CC stream (CC102 in the example).

*CC102*: a midi track that takes the CC stream from the *Mic1-CC102* track
and forwards it to the track where **FLOOR** device lives.


![PatchPresentation](https://docs.google.com/drawings/d/e/2PACX-1vTbjXY7gcTSEtnhRpsmyUEnHcRI6nvfGY9ZFM_A1p9jadAuKBLcKITwEAefPNUqLHMbPUoC9G7nmlv_/pub?w=410&h=774)


### General envelope ###

All the mics are set to 'Sends only' (no output), and directed towards a Return track called 'AllMics'.
That way, a global envelope follower can be set in an audio track (*AllMics-CC116* in the example)
which is mapped to another instance of
[**Tesser_CC2note**](https://bitbucket.org/AdrianArtacho/tesser_cc2note/src/master/).
That yields a midi CC stream (in our example CC106)
that is forwarded to the 
[**Floor**](https://bitbucket.org/AdrianArtacho/floor/src/master/) device
(inside the *FLOOR* track). 
One could hypothetically set different filters to process the audio signal before the envelope follower, 
to avoid ambient noise or a specific resonance frequency, for example.

![repo:R107:Allmics](https://docs.google.com/drawings/d/e/2PACX-1vTSRRI1T3lxT9kvbjH87-0yVPUTbTv3GliQFjXr4fqkzzREHbBsOIB5Vr7lJfT8Q9Qx1wn4XzP62GUb/pub?w=800&h=844)


## Settings ##

In the current version of the device, only the aggregate envelope (CC116) is considered.
The **threshold** value can be set to the intensity at which a *peak* is detected.
Below the orange **peak** monitor, there is the minimum time (in seconds) between 2 consecutive peaks.
If set to very low, peaks will constantly be triggering lights and launching clips (!).

The **steps** value (by default 8) is the amount of steps 
between the threshold, and the maximum envelope value.
The **exponential base** variable
helps curb the measured peaks in a way that make interaction more intuitive
(as to walking on the mics).


## Modi ##

In *OFF* mode, the device does not process incoming envelope values
and therefore doesn't trigger anything.

In *Inactive* Mode,
the device sends out midinotes (1-8, according to the numnber of steps)
that could trigger different things:

* Trigger light preset (when the midinote output is forwarded to the *PRESET* track.
* Trigger clip launching (when the midinote output is forwarded to the midi input of a [Rocket](https://bitbucket.org/AdrianArtacho/rocket/src/master/).

In *Active* Mode, 
the device does a similar calculation,
but then, for each of the steps it chooses randomly between 5
different alternatives for each step (**A**,**B**, **C**, **D** and **E**).
Therefore, a peak of intensity 1 (from the 8 possible steps)
could yield any of the following midinotes:

- Step 1, alternative A: midinote **1**
- Step 1, alternative B: midinote **2**
- Step 1, alternative C: midinote **3**
- Step 1, alternative D: midinote **4**
- Step 1, alternative E: midinote **5**
- Step 2, alternative A: midinote **6**
- Step 2, alternative B: midinote **7**
etc.

### Selecting between (IN)Active clips ###

There are 2 tracks, each with a 
[Tesser_clips](https://bitbucket.org/AdrianArtacho/tesser_clips)
device, each pointing at a different track (themselves pointing at the *PRESETS* track).



![repo:R107:floorbus](https://docs.google.com/drawings/d/e/2PACX-1vTsrogq4ichCNLXzDVnKavcanchBr7cfwPNu0VZkCX_D_X2mWz50VqrvinUKX6rgPBfOrJ9sODe1zKT/pub?w=1484&h=342)

## Triggering sequences of notes ##

As mentioned before, the midinote output of the **Floor** device can be 
used to trigger clips in a 
[Rocket](https://bitbucket.org/AdrianArtacho/rocket/src/master/)
or light presets (sent directly to the *PRESETS* track).
It can also launch slots where there is a clip with a sequence of midinotes,
using the
[Tesser_Clips](https://bitbucket.org/AdrianArtacho/tesser_clips) M4L device.


## Controlling chains ##

* Turn ON/OFF different chains in a Rack, provided that a [META_Knob](https://bitbucket.org/AdrianArtacho/meta_knob/src/master/) receives the midi CC stream frok the **Floor** object and has the CC22,23,24,25,104,105,106,107 mapped onto the different chain gains)


## Future work ##

You can run simulations with no microphones, using pink noise generator and two delays:

* Set mode to "noise with delay simulation"
* move up and down the delay length (ms) of the second simulated input.
* Enjoy!

### Who do I talk to? ###

The measurement part of the patch is largely based on [Edwin Van der Heide's](https://cycling74.com/author/5465182f06e117ca5ee443b6) original [patch](https://cycling74.com/forums/piezo-triangulation).
The triangulation method is original.

Contact: adrian.artacho@gmail.com